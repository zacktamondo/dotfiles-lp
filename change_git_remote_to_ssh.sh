#! /usr/bin/sh
echo "Before applying:"
git remote -v

git remote set-url origin git@codeberg.org:zacktamondo/dotfiles-lp.git

echo "---"
echo "after applying:"
git remote -v
