#!/bin/bash

CONFIG="$HOME/.config/hypr/wofi-full/config"
STYLE="$HOME/.config/hypr/wofi-full/style.css"
COLORS="$HOME/.config/hypr/wofi-full/colors"

if [[ ! $(pidof wofi) ]]; then
	wofi --show drun --prompt 'Search...' --conf ${CONFIG} --style ${STYLE} --color ${COLORS}
else
	pkill wofi
fi
