#!/bin/bash

exec ~/.config/hypr/scripts/launch.sh &

pypr &
exec ~/.config/hypr/scripts/suspend.sh &
dunst & 

exec cliphist wipe &
exec wl-paste --type text --watch cliphist store & #Stores only text data
exec wl-paste --type image --watch cliphist store & #Stores only image data

swaybg -m fill -i Pictures/Wallpapers/Darknet_diaries/Dangling_desktop.jpg &
/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &
variety &
nm-applet &
blueberry-tray &
syncthing -no-browser &
/usr/bin/emacs --daemon &
/usr/lib/pam_kwallet_init &
