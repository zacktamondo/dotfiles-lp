#!/bin/bash

# Colors
bg='1e1e2e' bgl='313244' fg='d9e0ee'
red='f38ba8' green='a6e3a1' yellow='f9e2af'
blue='89b4fa' magenta='cba6f7' cyan='89dceb'
alpha='ffffffff'

# CMD
swaylock -f \
	`# General` \
	--ignore-empty-password \
	--show-failed-attempts \
	--hide-keyboard-layout \
	--indicator-caps-lock \
	`# Blur screenshot` \
  --screenshots --clock --indicator-idle-visible \
  --effect-scale 0.5 --effect-blur 7x3 --effect-scale 2 \
	--effect-vignette 0.5:0.5 \
	`# Appearance - change CC to your desired transparency` \
	--color ${bg}FF \
	`#--image ${IMAGE} --scaling fill` \
	--font 'FiraCode Nerd Font' --font-size 30 \
	`# Backspace Key` \
	--key-hl-color ${green} \
	--caps-lock-key-hl-color ${blue} \
	--bs-hl-color ${red} \
	--caps-lock-bs-hl-color ${red} \
	`# Indicator` \
	--indicator-radius 120 \
	--indicator-thickness 10 \
	`# Inside Circle Colors` \
	--inside-color ${bg} \
	--inside-clear-color ${bg} \
	--inside-caps-lock-color ${bg} \
	--inside-ver-color ${blue} \
	--inside-wrong-color ${red} \
	`# Layout Colors` \
	--layout-bg-color ${cyan} \
	--layout-border-color ${cyan} \
	--layout-text-color ${bg} \
	`# Line Colors` \
	--line-color ${bg} \
	--line-clear-color ${red} \
	--line-caps-lock-color ${bg} \
	--line-ver-color ${bg} \
	--line-wrong-color ${bg} \
	`# Ring Colors` \
	--ring-color ${cyan} \
	--ring-clear-color ${bg} \
	--ring-caps-lock-color ${magenta} \
	--ring-ver-color ${blue} \
	--ring-wrong-color ${red} \
	`# Separator Color` \
	--separator-color ${bg} \
	`# Text Colors` \
	--text-color ${fg} \
	--text-clear-color ${fg} \
	--text-caps-lock-color ${fg} \
	--text-ver-color ${bg} \
	--text-wrong-color ${bg} \
	--indicator-caps-lock
