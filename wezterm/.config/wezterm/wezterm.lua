-- Pull in the wezterm API
local wezterm = require("wezterm")

-- This table will hold the configuration.
local config = {}

-- In newer versions of wezterm, use the config_builder which will
-- help provide clearer error messages
if wezterm.config_builder then
	config = wezterm.config_builder()
end

-- This is where you actually apply your config choices

config.color_scheme = "Tokyo Night"
-- config.color_scheme = 'nord'

config.window_background_opacity = 0.9
-- config.window_background_image = '/home/zacktamondo/Documents/Wallpaper/owl.jpg'
-- config.window_background_image_hsb = {
--   -- Darken the background image by reducing it to 1/3rd
--   brightness = 0.04,

--   -- You can adjust the hue by scaling its value.
--   -- a multiplier of 1.0 leaves the value unchanged.
--   hue = 1.0,

--   -- You can adjust the saturation also.
--   saturation = 1.0,
-- }

config.enable_tab_bar = false
config.window_padding = {
	left = "0cell",
	right = "0cell",
	top = "0cell",
	bottom = "-1cell",
}

config.font = wezterm.font("FiraCode Nerd Font")
config.font_size = 18
config.bidi_enabled = true
-- config.bidi_direction = 'RightToLeft'

config.enable_wayland = true

local mux = wezterm.mux
wezterm.on("gui-startup", function(cmd)
	local tab, pane, window = mux.spawn_window(cmd or {})
	window:gui_window():maximize()
end)
-- and finally, return the configuration to wezterm
return config
