#!/bin/sh

# Output color:
Light_Blue='\033[1;34m'
cd ~/Code/web_projects
read -p "Enter the git link: " git_link
read -p "Enter the name of the project: " project_name
project_name_u="${project_name^}"

git clone $git_link $project_name
echo -e "${Light_Blue}Clone finished"

cd $project_name

composer install
echo -e "${Light_Blue}Composer install finished"

cp .env.example .env
echo -e "${Light_Blue}.env created"

sed -i -e "s/APP_NAME=Laravel/APP_NAME=$project_name_u/g" \
-e "s/APP_URL=http:\/\/localhost/APP_URL=https:\/\/$project_name.test/g" \
-e "s/DB_DATABASE=filament_template/DB_DATABASE=${project_name}_db/g" \
-e "s/DB_PASSWORD=/DB_PASSWORD=pass/g" .env
echo -e "${Light_Blue}.env updated"

sed -i -e "s/$company_name = 'filament-template';/$company_name = '$project_name';/g" database/seeders/SettingSeeder.php
echo -e "${Light_Blue}Settings Seeder updated"

pass=$(pwgen -c 16 1)
sed -i -e "s/'password' => Hash::make('Cling8-Backlog-Glorifier'),/'password' => Hash::make('$pass'),/g" database/seeders/UserSeeder.php
echo -e "${Light_Blue}User Seeder updated"

# Stage file and commit
git commit -am "Update SettingSeeder & Admin password"

php artisan migrate
echo -e "${Light_Blue}Database created"
php artisan migrate:fresh --seed
echo -e "${Light_Blue}Database created with seeds"

php artisan shield:super-admin
echo -e "${Light_Blue}Super-Admin activated"

php artisan key:generate
echo -e "${Light_Blue}App key generated"

npm install
npm run build
echo -e "${Light_Blue}NPM packages built"

echo -e "${Light_Blue}Enter Root password for SSL:"
valet secure
echo -e "${Light_Blue}Valet secure finished"

librewolf $project_name.test/admin
nvim draft.yaml
