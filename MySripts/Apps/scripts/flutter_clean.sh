#!/bin/sh

# Move to lib/features directory
cd lib/features

# Prompt the user for the feature name
read -p "Enter the name of the feature: " feature_name

# Create the feature folder and all subfolders in one command
mkdir -p $(
echo "$feature_name/data/models" 
echo "$feature_name/data/repositories" 
echo "$feature_name/data/data_sources" 

echo "$feature_name/domain/entities" 
echo "$feature_name/domain/repositories" 
echo "$feature_name/domain/usecases" 

echo "$feature_name/presentation/pages"
echo "$feature_name/presentation/widgets" 
echo "$feature_name/presentation/bloc"
)

# Print a message to confirm that the feature folder has been created
echo "Feature folder '$feature_name' has been created."
