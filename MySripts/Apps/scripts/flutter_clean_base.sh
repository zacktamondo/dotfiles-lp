#!/bin/sh

# Move to lib folder
cd lib

# Create the base folders in one command
mkdir -p $(
echo "features"

echo "config/routes"
echo "config/theme"

echo "core/error"
echo "core/network"
echo "core/usecases"
echo "core/utils"
echo "core/resources"
)

# Print a message to confirm that the folders has been created
echo "Base clean architecture folders has been created."
