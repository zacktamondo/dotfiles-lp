#!/bin/sh

# Prompt the user for the database name
read -p "Enter the name of the project: " project_name

# Make variables to follow my standard
database_name="${project_name}_db"
database_user_name="${project_name}_user_db"
database_user_password="${project_name}123"

echo "database_name = $database_name"
echo "database_user_name = $database_user_name"
echo "database_user_password = $database_user_password"

# The SQL query
query="
CREATE DATABASE \`${database_name}\`; 
USE \`${database_name}\`;
CREATE USER \`${database_user_name}\`@localhost IDENTIFIED BY '\`${database_user_password}\`';
GRANT ALL PRIVILEGES ON \`${database_name}\`.* TO \`${database_user_name}\`@'localhost';
"

# Executing the query 
mysql -u root -p -s "$query"

  # Check if .env in the current directory
  if [ -f .env ]; then
    # .env exists
    read -p "Do you want to add database to your .env? (Y/n) " answer 
    # If user pressed enter
    if [ -z "$answer" ]; then
      answer="y"
    fi
    if [ "$answer" = "y" ] || [ "$answer" = "Y" ]; then
      sed -i "s/DB_DATABASE=.*/DB_DATABASE=$database_name/g" .env
      sed -i "s/DB_USERNAME=.*/DB_USERNAME=$database_user_name/g" .env
      sed -i "s/DB_PASSWORD=.*/DB_PASSWORD=$database_user_password/g" .env
    fi
  else
    # .env not found
    echo ".env not found in current directory"
    fi
