#           _              
#   _______| |__  _ __ ___ 
#  |_  / __| '_ \| '__/ __|
# _ / /\__ \ | | | | | (__ 
#(_)___|___/_| |_|_|  \___|

# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
# if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
#   source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
# fi
 
# Dependancies You Need for this Config
# zsh-syntax-highlighting - syntax highlighting for ZSH in standard repos
# autojump - jump to directories with j or jc for child or jo to open in file manager
# zsh-autosuggestions - Suggestions based on your history

# Initial Setup
# mkdir -p "$HOME/.zsh"
# git clone https://github.com/sindresorhus/pure.git "$HOME/.zsh/pure"
# Setup Alias in $HOME/.zsh/aliasrc

# Enable colors and change prompt:
autoload -U colors && colors
PS1="%B%{$fg[red]%}[%{$fg[yellow]%}%n%{$fg[green]%}@%{$fg[blue]%}%M %{$fg[magenta]%}%~%{$fg[red]%}]%{$reset_color%}$%b "

# nvims
if [[   -f "$HOME/.config/nvims/nvim_appnames" ]]; then
	source "$HOME/.config/nvims/nvim_appnames"
fi

# Pure Prompt
#fpath+=$HOME/.zsh/pure
#autoload -U promptinit; promptinit
#prompt pure

# Terminal navigation functions and aliases using fzf
j(){
  cd "$(find -type d | fzf)"
}
jp(){
  cd "$(find ~/Code -maxdepth 2 -type d  | fzf)"
}
e(){
  xdg-open"$(find -type f | fzf)"
}
alias gfp="(find -type f | fzf | sed 's/^..//g' | tr -d '\n' | xclip -selection c)"
alias gdp="(find -type d | fzf | sed 's/^..//g' | tr -d '\n' | xclip -selection c)"

# This is the list for lf icons:
[ -f "$HOME/.config/lf/icons" ] && source "$HOME/.config/lf/icons"

# Set up fzf key bindings and fuzzy completion
source <(fzf --zsh)

# History in cache directory:
HISTSIZE=10000
SAVEHIST=10000
HISTFILE=~/.zshhistory
setopt INC_APPEND_HISTORY        # Write to the history file immediately, not when the shell exits.
setopt SHARE_HISTORY             # Share history between all sessions.
setopt HIST_EXPIRE_DUPS_FIRST    # Expire duplicate entries first when trimming history.
setopt HIST_IGNORE_DUPS          # Don't record an entry that was just recorded again.
setopt HIST_IGNORE_ALL_DUPS      # Delete old recorded entry if new entry is a duplicate.
setopt HIST_FIND_NO_DUPS         # Do not display a line previously found.
setopt autocd		             # Automatically cd into typed directory.
stty stop undef		             # Disable ctrl-s to freeze terminal.

# Basic auto/tab complete:
autoload -U compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
eval "$(zoxide init zsh)"
_comp_options+=(globdots)               # Include hidden files.

# Custom ZSH Binds
bindkey '^ ' autosuggest-accept

bindkey '^[[A' history-substring-search-up
bindkey '^[[B' history-substring-search-down
bindkey -M vicmd 'k' history-substring-search-up
bindkey -M vicmd 'j' history-substring-search-down

# Load aliases and shortcuts if existent.
[ -f "$HOME/.zsh/aliasrc" ] && source "$HOME/.zsh/aliasrc"

#enable vim mode in zsh
bindkey -v

# Enable search history using Ctrl + R
#bindkey '^R' history-incremental-search-backward

# Flutter path
export PATH="$PATH:$HOME/Android/flutter/bin"
export PATH="$PATH:$HOME/.pub-cache/bin"
#export CHROME_EXECUTABLE=/usr/bin/google-chrome-stable 
export CHROME_EXECUTABLE=/usr/bin/brave 
export PATH="$PATH:$HOME/.config/composer/vendor/bin"                
export PATH="$PATH:$HOME/.local/bin"                
export PATH="$PATH:$HOME/Apps/scripts"                
export PATH="$PATH:$HOME/.npm-global/bin"                
export PATH="$PATH:$HOME/.emacs.doom/bin"                

# XDG Cleaning
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_STATE_HOME="$HOME/.local/state"
export XDG_CACHE_HOME="$HOME/.cache"
#export ANDROID_HOME="$XDG_DATA_HOME"/android

export DOOMDIR="$HOME/.doom.d/"

export NPM_CONFIG_PREFIX="$HOME/.npm-global"
# pnpm
export PNPM_HOME="$HOME/.local/share/pnpm"
case ":$PATH:" in
  *":$PNPM_HOME:"*) ;;
  *) export PATH="$PNPM_HOME:$PATH" ;;
esac
# pnpm end

# NNN config
export NNN_FIFO="/tmp/nnn.fifo" 
export NNN_PLUG="f:finder;o:xdgdefault"

source /usr/share/zsh-theme-powerlevel10k/powerlevel10k.zsh-theme

#lvim
export LUNARVIM_RUNTIME_DIR="${LUNARVIM_RUNTIME_DIR:-"$HOME/.local/share/lunarvim"}"
export LUNARVIM_CONFIG_DIR="${LUNARVIM_CONFIG_DIR:-"$HOME/.config/lvim"}"
export LUNARVIM_CACHE_DIR="${LUNARVIM_CACHE_DIR:-"$HOME/.cache/lvim"}"
export LUNARVIM_BASE_DIR="${LUNARVIM_BASE_DIR:-"$HOME/.local/share/lunarvim/lvim"}"

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

# Load ; should be last.
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh 2>/dev/null
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh 2>/dev/null
source /usr/share/zsh/plugins/zsh-history-substring-search/zsh-history-substring-search.zsh 2>/dev/null


