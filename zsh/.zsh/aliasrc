#
# # ex - archive extractor
# # usage: ex <file>
ex ()
{
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)   tar xjf $1   ;;
      *.tar.gz)    tar xzf $1   ;;
      *.bz2)       bunzip2 $1   ;;
      *.rar)       unrar x $1     ;;
      *.gz)        gunzip $1    ;;
      *.tar)       tar xf $1    ;;
      *.tbz2)      tar xjf $1   ;;
      *.tgz)       tar xzf $1   ;;
      *.zip)       unzip $1     ;;
      *.Z)         uncompress $1;;
      *.7z)        7z x $1      ;;
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}


# Defaults
export EDITOR='nvim'
export BROWSER='librewolf'
export TERM='tmux-256color'

# Aliases
alias pacman-update='sudo pacman-mirrors --geoip'
alias zshrc='${=EDITOR} ~/.zshrc' # Quick access to the ~/.zshrc file
alias grep='grep --color'
alias sgrep='grep -R -n -H -C 5 --exclude-dir={.git,.svn,CVS} '
alias t='tail -f'

# ls, the common ones I use a lot shortened for rapid fire usage
alias ls='lsd --color=auto'
alias ll='lsd -l'
alias l='lsd -lFh --color=auto'     #size,show type,human readable
alias la='lsd -lAFh --color=auto'   #long list,show almost all,show type,human readable
alias lr='lsd -tRFh --color=auto'   #sorted by date,recursive,show type,human readable
alias lt='lsd -ltFh --color=auto'   #long list,sorted by date,show type,human readable
alias ll='lsd -l --color=auto'      #long list
alias ldot='lsd -ld .* --color=auto'
alias lS='lsd -1FSsh --color=auto'
alias lart='lsd -1Fcart --color=auto'
alias lrt='lsd -1Fcrt --color=auto'

# Command line head / tail shortcuts
alias -g H='| head'
alias -g T='| tail'
alias -g G='| grep'
alias -g L="| less"
alias -g M="| most"
alias -g LL="2>&1 | less"
alias -g CA="2>&1 | cat -A"
alias -g NE="2> /dev/null"
alias -g NUL="> /dev/null 2>&1"
alias -g P="2>&1| pygmentize -l pytb"

alias dud='du -d 1 -h'
alias duf='du -sh *'
alias fd='find . -type d -name'
alias ff='find . -type f -name'

alias h='history'
alias hgrep="fc -El 0 | grep"
alias help='man'
alias p='ps -f'
alias sortnr='sort -n -r'
alias unexport='unset'

alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'
alias vim='nvim'
alias nano='vim'
alias signal-desktop='signal-desktop --use-tray-icon'
alias pa='php artisan'
