local ToggleTerm = {

  bashtop = function()
    local Terminal = require('toggleterm.terminal').Terminal
    local t = Terminal:new { cmd = 'bashtop', direction = 'float' }
    return t:toggle()
  end,

  lazygit = function()
    local Terminal = require('toggleterm.terminal').Terminal
    local lazygit = Terminal:new {
      cmd = 'lazygit',
      hidden = true,
      direction = 'float',
      float_opts = {
        border = 'none',
        width = 100000,
        height = 100000,
        zindex = 200,
      },
      on_open = function(_)
        vim.cmd 'startinsert!'
      end,
      on_close = function(_) end,
      count = 99,
    }
    return lazygit:toggle()
  end,

  vertical = function()
    local Terminal = require('toggleterm.terminal').Terminal
    local t = Terminal:new { size = 0.4, direction = 'vertical' }
    return t:toggle()
  end,

  horizontal = function()
    local Terminal = require('toggleterm.terminal').Terminal
    local t = Terminal:new { size = 0.3, direction = 'horizontal' }
    return t:toggle()
  end,

  float = function()
    local Terminal = require('toggleterm.terminal').Terminal
    local t = Terminal:new { direction = 'float' }
    return t:toggle()
  end,

  neomutt = function()
    local Terminal = require('toggleterm.terminal').Terminal
    local t = Terminal:new { cmd = 'neomutt', direction = 'float' }
    return t:toggle()
  end,

  ranger = function()
    local Terminal = require('toggleterm.terminal').Terminal
    local t = Terminal:new { cmd = 'ranger', direction = 'float' }
    return t:toggle()
  end,
}

return ToggleTerm
