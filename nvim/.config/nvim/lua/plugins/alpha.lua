return {
  'goolord/alpha-nvim',
  dependencies = {
    'nvim-tree/nvim-web-devicons',
  },

  config = function()
    local alpha = require 'alpha'
    local dashboard = require 'alpha.themes.dashboard'

    dashboard.section.header.val = {
      '           ▄ ▄                   ',
      '       ▄   ▄▄▄     ▄ ▄▄▄ ▄ ▄     ',
      '       █ ▄ █▄█ ▄▄▄ █ █▄█ █ █     ',
      '    ▄▄ █▄█▄▄▄█ █▄█▄█▄▄█▄▄█ █     ',
      '  ▄ █▄▄█ ▄ ▄▄ ▄█ ▄▄▄▄▄▄▄▄▄▄▄▄▄▄  ',
      '  █▄▄▄▄ ▄▄▄ █ ▄ ▄▄▄ ▄ ▄▄▄ ▄ ▄ █ ▄',
      '▄ █ █▄█ █▄█ █ █ █▄█ █ █▄█ ▄▄▄ █ █',
      '█▄█ ▄ █▄▄█▄▄█ █ ▄▄█ █ ▄ █ █▄█▄█ █',
      '    █▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄█ █▄█▄▄▄█    ',
    }
    dashboard.section.buttons.val = {
      dashboard.button('f', '' .. '  Find File', '<CMD>Telescope find_files<CR>'),
      dashboard.button('n', '' .. '  New File', '<CMD>ene!<CR>'),
      dashboard.button('p', '' .. '  Projects ', '<CMD>Telescope projects<CR>'),
      dashboard.button('r', '' .. '  Recent files', ':Telescope oldfiles <CR>'),
      dashboard.button('t', '' .. '  Find Text', '<CMD>Telescope live_grep<CR>'),
      dashboard.button('c', '' .. '  Configuration', '<CMD>edit ' .. '$MYVIMRC' .. ' <CR>'),
      dashboard.button('q', '' .. '  Quit', '<CMD>quit<CR>'),
    }

    alpha.setup(dashboard.opts)
  end,
}
