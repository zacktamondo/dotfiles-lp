#! /bin/bash 
picom &
nitrogen --restore &
urxvtd -q -o -f &
setxkbmap -option grp:switch,grp:alt_shift_toggle us,ar
