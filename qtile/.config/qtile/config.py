# Copyright (c) 2010 Aldo Cortesi
# Copyright (c) 2010, 2014 dequis
# Copyright (c) 2012 Randall Ma
# Copyright (c) 2012-2014 Tycho Andersen
# Copyright (c) 2012 Craig Barnes
# Copyright (c) 2013 horsik
# Copyright (c) 2013 Tao Sauvage
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


import os
import re
import socket
import subprocess
from libqtile import qtile
from libqtile.config import Drag, Key, Screen, Group, Click, Rule, Match, ScratchPad, DropDown
from libqtile.lazy import lazy
from libqtile import layout, bar, widget, hook
from libqtile.widget import Spacer
#import arcobattery

# mod4 or mod = super key
mod = "mod4"
mod1 = "alt"
mod2 = "control"
home = os.path.expanduser('~')
myTerm = "wezterm"
myFileManager = "thunar"
myBrowser = "librewolf"
enableKeyboardLayout = "setxkbmap -option grp:switch,grp:alt_shift_toggle us,ara &"


@lazy.function
def window_to_prev_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i - 1].name)


@lazy.function
def window_to_next_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i + 1].name)


keys = [

    # FUNCTION KEYS

    #Key([], "F12", lazy.spawn('xfce4-terminal --drop-down')),

    # SUPER + FUNCTION KEYS

    Key([mod], "e", lazy.spawn(myFileManager)),
    Key([mod], "c", lazy.spawn('conky-toggle')),
    Key([mod], "f", lazy.window.toggle_fullscreen()),
    Key([mod], "q", lazy.window.kill()),
    Key([mod], "d", lazy.spawn('rofi -show drun')),
    Key([mod], "r", lazy.spawn('rofi -show drun')),
    Key([mod], "p", lazy.spawn('pavucontrol')),
    Key([mod], "Delete", lazy.spawn(enableKeyboardLayout)),
    Key([mod], "w", lazy.spawn(myBrowser)),
    Key([mod], "x", lazy.shutdown()),
    Key([mod], "Escape", lazy.spawn('xkill')),
    Key([mod], "Return", lazy.spawn(myTerm)),
    Key([mod], "KP_Enter", lazy.spawn('termite')),
    Key([mod], "F1", lazy.spawn(myBrowser)),
    Key([mod], "F2", lazy.spawn('emacsclient -c -a emacs')),
    Key([mod], "F3", lazy.spawn('brave')),
    Key([mod], "F4", lazy.spawn('gimp')),
    Key([mod], "F5", lazy.spawn('meld')),
    Key([mod], "F6", lazy.spawn('vlc --video-on-top')),
    Key([mod], "F7", lazy.spawn('virt-manager')),
    Key([mod], "F8", lazy.spawn(myFileManager)),
    Key([mod], "F9", lazy.spawn('evolution')),
    Key([mod], "F10", lazy.spawn("spotify")),
    Key([mod], "F11", lazy.spawn('rofi -show run -fullscreen')),
    Key([mod], "F12", lazy.spawn('rofi -show run')),

    # SUPER + SHIFT KEYS

    Key([mod, "shift"], "Return", lazy.spawn(myFileManager)),
    Key([mod, "shift"], "d", lazy.spawn(
        "dmenu_run -i -nb '#2e3440' -nf '#eeeeee' -sb '#81a1c1' -sf '#eeeeee' -fn 'FiraCode Nerd Font:bold:pixelsize=18'")),
    Key([mod, "shift"], "e", lazy.spawn("./Apps/dmenu\ Scripts/dmconf")),
    Key([mod, "shift"], "a", lazy.spawn('rofi -modi emoji -show emoji')),
    Key([mod, "shift"], "s", lazy.spawn('rofi -show power-menu -modi power-menu:./Apps/scripts/rofi-power-menu')),
    Key([mod, "shift"], "c", lazy.spawn('xfce4-clipman-history')),
    Key([mod, "shift"], "p", lazy.spawn(myBrowser + ' --private-window')),
    Key([mod, "shift"], "q", lazy.window.kill()),
    Key([mod, "shift"], "r", lazy.restart()),
    Key([mod, "control"], "r", lazy.restart()),
    #Key([mod, "shift"], "x", lazy.shutdown()),
    Key([mod, "shift"], "x", lazy.spawn("./Apps/dmenu\ Scripts/dmlogout")),

    # CONTROL + ALT KEYS

    Key(["mod1", "control"], "Next", lazy.spawn('conky-rotate -n')),
    Key(["mod1", "control"], "Prior", lazy.spawn('conky-rotate -p')),
    Key(["mod1", "control"], "a", lazy.spawn('xfce4-appfinder')),
    Key(["mod1", "control"], "b", lazy.spawn('thunar')),
    Key(["mod1", "control"], "c", lazy.spawn('catfish')),
    Key(["mod1", "control"], "e", lazy.spawn('arcolinux-tweak-tool')),
    Key(["mod1", "control"], "g", lazy.spawn(
        'chromium -no-default-browser-check')),
    Key(["mod1", "control"], "i", lazy.spawn('nitrogen')),
    Key(["mod1", "control"], "k", lazy.spawn('arcolinux-logout')),
    Key(["mod1", "control"], "l", lazy.spawn('arcolinux-logout')),
    Key(["mod1", "control"], "m", lazy.spawn('xfce4-settings-manager')),
    Key(["mod1", "control"], "o", lazy.spawn(
        home + '/.config/qtile/scripts/picom-toggle.sh')),
    Key(["mod1", "control"], "p", lazy.spawn('pamac-manager')),
    Key(["mod1", "control"], "r", lazy.spawn('rofi-theme-selector')),
    Key(["mod1", "control"], "s", lazy.spawn('spotify')),
    Key(["mod1", "control"], "t", lazy.spawn('termite')),
    Key(["mod1", "control"], "u", lazy.spawn('pavucontrol')),
    Key(["mod1", "control"], "v", lazy.spawn('vivaldi-stable')),
    Key(["mod1", "control"], "w", lazy.spawn('arcolinux-welcome-app')),
    Key(["mod1", "control"], "Return", lazy.spawn('termite')),

    # ALT + ... KEYS

    Key(["mod1"], "f", lazy.spawn('variety -f')),
    Key(["mod1"], "n", lazy.spawn('variety -n')),
    Key(["mod1"], "p", lazy.spawn('variety -p')),
    Key(["mod1"], "t", lazy.spawn('variety -t')),
    Key(["mod1"], "Up", lazy.spawn('variety --pause')),
    Key(["mod1"], "Down", lazy.spawn('variety --resume')),
    Key(["mod1"], "Left", lazy.spawn('variety -p')),
    Key(["mod1"], "Right", lazy.spawn('variety -n')),
    Key(["mod1"], "F2", lazy.spawn('gmrun')),
    Key(["mod1"], "F3", lazy.spawn('xfce4-appfinder')),
    Key(["mod1"], "space", lazy.spawn('krunner')),

    # VARIETY KEYS WITH PYWAL

    Key(["mod1", "shift"], "f", lazy.spawn(
        home + '/.config/qtile/scripts/set-pywal.sh -f')),
    Key(["mod1", "shift"], "p", lazy.spawn(
        home + '/.config/qtile/scripts/set-pywal.sh -p')),
    Key(["mod1", "shift"], "n", lazy.spawn(
        home + '/.config/qtile/scripts/set-pywal.sh -n')),
    Key(["mod1", "shift"], "u", lazy.spawn(
        home + '/.config/qtile/scripts/set-pywal.sh -u')),

    # CONTROL + SHIFT KEYS

    Key([mod2, "shift"], "Escape", lazy.spawn('xfce4-taskmanager')),

    # SCREENSHOTS

    Key([], "Print", lazy.spawn(
        "scrot 'ArcoLinux-%Y-%m-%d-%s_screenshot_$wx$h.jpg' -e 'mv $f $$(xdg-user-dir PICTURES)'")),
    Key([mod2], "Print", lazy.spawn('xfce4-screenshooter')),
    Key([mod2, "shift"], "Print", lazy.spawn('flameshot gui')),

    # MULTIMEDIA KEYS

    # INCREASE/DECREASE BRIGHTNESS
    Key([], "XF86MonBrightnessUp", lazy.spawn("brillo -q -A 5")),
    Key([], "XF86MonBrightnessDown", lazy.spawn("brillo -q -U 5")),

    # INCREASE/DECREASE/MUTE VOLUME
    Key([], "XF86AudioMute", lazy.spawn("amixer -q set Master toggle")),
    Key([], "XF86AudioLowerVolume", lazy.spawn("amixer -q set Master 5%-")),
    Key([], "XF86AudioRaiseVolume", lazy.spawn("amixer -q set Master 5%+")),

    Key([], "XF86AudioPlay", lazy.spawn("playerctl play-pause")),
    Key([], "XF86AudioNext", lazy.spawn("playerctl next")),
    Key([], "XF86AudioPrev", lazy.spawn("playerctl previous")),
    Key([], "XF86AudioStop", lazy.spawn("playerctl stop")),

    #    Key([], "XF86AudioPlay", lazy.spawn("mpc toggle")),
    #    Key([], "XF86AudioNext", lazy.spawn("mpc next")),
    #    Key([], "XF86AudioPrev", lazy.spawn("mpc prev")),
    #    Key([], "XF86AudioStop", lazy.spawn("mpc stop")),

    # QTILE LAYOUT KEYS
    Key([mod], "n", lazy.layout.normalize()),
    Key([mod], "space", lazy.next_layout()),

    # CHANGE FOCUS
    Key([mod], "Up", lazy.layout.up()),
    Key([mod], "Down", lazy.layout.down()),
    Key([mod], "Left", lazy.layout.left()),
    Key([mod], "Right", lazy.layout.right()),
    Key([mod], "k", lazy.layout.up()),
    Key([mod], "j", lazy.layout.down()),
    Key([mod], "h", lazy.layout.left()),
    #Key([mod], "l", lazy.layout.right()),
    Key([mod], "l", lazy.spawn("xflock4")),


    # RESIZE UP, DOWN, LEFT, RIGHT
    Key([mod, "control"], "l",
        lazy.layout.grow_right(),
        lazy.layout.grow(),
        lazy.layout.increase_ratio(),
        lazy.layout.delete(),
        ),
    Key([mod, "control"], "Right",
        lazy.layout.grow_right(),
        lazy.layout.grow(),
        lazy.layout.increase_ratio(),
        lazy.layout.delete(),
        ),
    Key([mod, "control"], "h",
        lazy.layout.grow_left(),
        lazy.layout.shrink(),
        lazy.layout.decrease_ratio(),
        lazy.layout.add(),
        ),
    Key([mod, "control"], "Left",
        lazy.layout.grow_left(),
        lazy.layout.shrink(),
        lazy.layout.decrease_ratio(),
        lazy.layout.add(),
        ),
    Key([mod, "control"], "k",
        lazy.layout.grow_up(),
        lazy.layout.grow(),
        lazy.layout.decrease_nmaster(),
        ),
    Key([mod, "control"], "Up",
        lazy.layout.grow_up(),
        lazy.layout.grow(),
        lazy.layout.decrease_nmaster(),
        ),
    Key([mod, "control"], "j",
        lazy.layout.grow_down(),
        lazy.layout.shrink(),
        lazy.layout.increase_nmaster(),
        ),
    Key([mod, "control"], "Down",
        lazy.layout.grow_down(),
        lazy.layout.shrink(),
        lazy.layout.increase_nmaster(),
        ),


    # FLIP LAYOUT FOR MONADTALL/MONADWIDE
    Key([mod, "shift"], "f", lazy.layout.flip()),

    # FLIP LAYOUT FOR BSP
    Key([mod, "mod1"], "k", lazy.layout.flip_up()),
    Key([mod, "mod1"], "j", lazy.layout.flip_down()),
    Key([mod, "mod1"], "l", lazy.layout.flip_right()),
    Key([mod, "mod1"], "h", lazy.layout.flip_left()),

    # MOVE WINDOWS UP OR DOWN BSP LAYOUT
    Key([mod, "shift"], "k", lazy.layout.shuffle_up()),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down()),
    Key([mod, "shift"], "h", lazy.layout.shuffle_left()),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right()),

    # MOVE WINDOWS UP OR DOWN MONADTALL/MONADWIDE LAYOUT
    Key([mod, "shift"], "Up", lazy.layout.shuffle_up()),
    Key([mod, "shift"], "Down", lazy.layout.shuffle_down()),
    Key([mod, "shift"], "Left", lazy.layout.swap_left()),
    Key([mod, "shift"], "Right", lazy.layout.swap_right()),

    # TOGGLE FLOATING LAYOUT
    Key([mod, "shift"], "space", lazy.window.toggle_floating()), ]

groups = []

# FOR QWERTY KEYBOARDS
group_names = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0", ]

# FOR AZERTY KEYBOARDS
#group_names = ["ampersand", "eacute", "quotedbl", "apostrophe", "parenleft", "section", "egrave", "exclam", "ccedilla", "agrave",]

#group_labels = ["1 ", "2 ", "3 ", "4 ", "5 ", "6 ", "7 ", "8 ", "9 ", "0",]
#group_labels = ["", "", "", "", "", "", "", "", "", "", ]
group_labels = ["", "", "", "", "", "", "", "", "", "", ]
#group_labels = ["Web", "Edit/chat", "Image", "Gimp", "Meld", "Video", "Vb", "Files", "Mail", "Music",]

group_layouts = ["monadtall", "monadtall", "monadtall", "monadtall",
                 "monadtall", "monadtall", "monadtall", "monadtall", "monadtall", "monadtall", ]
#group_layouts = ["monadtall", "matrix", "monadtall", "bsp", "monadtall", "matrix", "monadtall", "bsp", "monadtall", "monadtall",]

for i in range(len(group_names)):
    groups.append(
        Group(
            name=group_names[i],
            layout=group_layouts[i].lower(),
            label=group_labels[i],
        ))

for i in groups:
    keys.extend([

        # CHANGE WORKSPACES
        Key([mod], i.name, lazy.group[i.name].toscreen(toggle=True)),
        Key(["mod1"], "Tab", lazy.screen.next_group()),
        Key(["mod1", "shift"], "Tab", lazy.screen.prev_group()),
        # Switch focus of monitors
        Key([mod], "Tab", lazy.next_screen(), desc='Move focus to next monitor'),
        Key([mod, "shift"], "Tab", lazy.prev_screen(), desc='Move focus to prev monitor'),

        # MOVE WINDOW TO SELECTED WORKSPACE 1-10 AND STAY ON WORKSPACE
        #Key([mod, "shift"], i.name, lazy.window.togroup(i.name)),
        # MOVE WINDOW TO SELECTED WORKSPACE 1-10 AND FOLLOW MOVED WINDOW TO WORKSPACE
        Key([mod, "shift"], i.name, lazy.window.togroup(
            i.name), lazy.group[i.name].toscreen(toggle=True)),
    ])
groups.append(
    ScratchPad("scratchpad", [
        # define a drop down terminal.
        # it is placed in the upper third of screen by default.
        DropDown("terminal", myTerm, opacity=0.88, height=0.55, width=0.80, match=myTerm ),

        DropDown("protonVpn", "protonvpn", opacity=1.00, x= 0.4, y=0.1, width=0.3 , height=0.7, on_focus_lost_hide=True,),
        DropDown("telegram", "telegram-desktop", opacity=1.00, x= 0.1, y=0.1, width=0.3 , height=0.7, on_focus_lost_hide=True,),
        DropDown("signal", "signal-desktop --use-tray-icon", opacity=1.00, x= 0.1, y=0.1, width=0.3 , height=0.7, on_focus_lost_hide=True,),
        DropDown("mobile", "scrcpy", opacity=1.00, x= 0.01, y=0, width=0.25 , height=0.95, on_focus_lost_hide=True,),
        DropDown("notes", "./.joplin/Joplin.AppImage  %u", opacity=1.00, x= 0, y=0, width=1.0 , height=1.0, on_focus_lost_hide=True,),
        DropDown("surfsharkVPN", "surfshark", opacity=1.00, x= 0, y=0, width=1.0 , height=1.0, on_focus_lost_hide=True,),

        DropDown("calc", "galculator",
                 x=0.8, y=0.4, width=0.1, height=0.1, opacity=0.9,
                 on_focus_lost_hide=True),
    ]), )

keys.extend([
    # Scratchpad
    # toggle visibiliy of above defined DropDown named "term"
    Key([], 'F12', lazy.group['scratchpad'].dropdown_toggle('terminal')),
    Key([], 'F11', lazy.group['scratchpad'].dropdown_toggle('calc')),
    Key([mod], "b", lazy.group['scratchpad'].dropdown_toggle('protonVpn')),
    Key([mod], "t", lazy.group['scratchpad'].dropdown_toggle('telegram')),
    Key([mod], "s", lazy.group['scratchpad'].dropdown_toggle('signal')),
    Key([mod], "m", lazy.group['scratchpad'].dropdown_toggle('mobile')),
    Key([mod], "o", lazy.group['scratchpad'].dropdown_toggle('notes')),
    Key([mod], "v", lazy.group['scratchpad'].dropdown_toggle('surfsharkVPN')),
])


def init_layout_theme():
    return {"margin": 8,
            "border_width": 5,
            "border_focus": "#7aa2f7ee",
            "border_normal": "#414868ee"
            }


layout_theme = init_layout_theme()


layouts = [
    layout.MonadTall(**layout_theme),
    layout.MonadWide(**layout_theme),
    layout.Matrix(**layout_theme),
    layout.Bsp(**layout_theme),
    layout.Floating(**layout_theme),
    layout.RatioTile(**layout_theme),
    layout.Max(**layout_theme)
]

# COLORS FOR THE BAR


def init_colors():
    return [["#2F343F", "#2F343F"],  # color 0
            ["#2F343F", "#2F343F"],  # color 1
            ["#c0c5ce", "#c0c5ce"],  # color 2
            ["#fba922", "#fba922"],  # color 3
            ["#3384d0", "#3384d0"],  # color 4
            ["#929aa9", "#929aa9"],  # color 5
            ["#e39400", "#e39400"],  # color 6
            ["#62FF00", "#62FF00"],  # color 7
            ["#81a1c1", "#81a1c1"],  # color 8
            ["#d8dee9", "#d8dee9"]]  # color 9


colors = init_colors()


# WIDGETS FOR THE BAR

def init_widgets_defaults():
    return dict(font="FiraCode Nerd FontSemi-Bold",
                fontsize=12,
                padding=2,
                background=colors[1])


widget_defaults = init_widgets_defaults()


def init_widgets_list():
    prompt = "{0}@{1}: ".format(os.environ["USER"],socket.gethostname())
    widgets_list = [
            widget.GroupBox(font="FontAwesome",
                fontsize=16,
                #margin_y=-1,
                #margin_x=0,
                #padding=6,
                #padding_x=5,
                borderwidth=0,
                disable_drag=True,
                active=colors[9],
                inactive=colors[5],
                rounded=False,
                highlight_method="text",
                this_current_screen_border=colors[8],
                foreground=colors[2],
                background=colors[1]
                ),
            widget.Sep(
                linewidth=0,
                padding=10,
                foreground=colors[2],
                background=colors[1]
                ),
            widget.TextBox(text='',
                padding=6,
                fontsize=16,
                foreground=colors[5]),
            widget.CurrentLayout(
                font="FiraCode Nerd Font Bold",
                foreground=colors[5],
                background=colors[1]
                ),
            widget.Sep(
                linewidth=0,
                padding=10,
                foreground=colors[2],
                background=colors[1]
                ),
            widget.TaskList(font="DejaVu Sans Mono",
                            padding_x=5,
                            fontsize=12,
                            foreground=colors[5],
                            background=colors[1],
                            ),
            widget.Pomodoro(font="FiraCode",
                            prefix_inactive="Inactive",
                            fontsize=12,
                            color_inactive=colors[5],
                            color_active=colors[3],
                            color_break=colors[4],
                            background=colors[1],
                            ),
            widget.Net(
                    font="FiraCode Nerd Font",
                    fontsize=12,
                    format=' {down:.0f} {down_suffix}  {up:.0f} {up_suffix}',
                    interface="wlan0",
                    mouse_callbacks={
                        'Button1': lambda qtile: qtile.cmd_spawn(myTerm + ' -e nmtui')},
                    foreground=colors[9],
                    background=colors[1],
                    padding=0,
                    ),
        widget.Sep(
                linewidth=0,
                padding=10,
                foreground=colors[2],
                background=colors[1]
                ),
        # widget.NetGraph(
        #          font="Noto Sans",
        #          fontsize=12,
        #          bandwidth="down",
        #          interface="auto",
        #          fill_color = colors[8],
        #          foreground=colors[2],
        #          background=colors[1],
        #          graph_color = colors[8],
        #          border_color = colors[2],
        #          padding = 0,
        #          border_width = 1,
        #          line_width = 1,
        #          ),
        widget.TextBox(text='墳',
                fontsize=16,
                foreground=colors[9]),
        widget.Volume(
                foreground=colors[9],
                background=colors[1],
                padding=5
                ),
        widget.Sep(
                linewidth=0,
                padding=10,
                foreground=colors[2],
                background=colors[1]
                ),
        widget.TextBox(text=' ',
                fontsize=20,
                foreground=colors[9]),
        widget.Battery(
                font="FiraCode",
                update_interval=10,
                charge_char='^',
                discharge_char='v',
                fontsize=12,
                foreground=colors[9],
                background=colors[1],
                ),
        widget.Sep(
                linewidth=0,
                padding=10,
                foreground=colors[2],
                background=colors[1]
                ),
        widget.TextBox(text='',
                fontsize=16,
                foreground=colors[9]),
        # # do not activate in Virtualbox - will break qtile
        widget.ThermalSensor(
                foreground=colors[9],
                foreground_alert=colors[6],
                background=colors[1],
                metric=True,
                padding=3,
                threshold=80
                ),
        widget.Sep(
                linewidth=0,
                padding=10,
                foreground=colors[2],
                background=colors[1]
                ),
        # # battery option 1  ArcoLinux Horizontal icons do not forget to import arcobattery at the top
        # arcobattery.BatteryIcon(
        #          padding=0,
        #          scale=0.7,
        #          y_poss=2,
        #          theme_path=home + "/.config/qtile/icons/battery_icons_horiz",
        #          update_interval = 5,
        #          background = colors[1]
        #          ),
        # # battery option 2  from Qtile
        # widget.Sep(
        #          linewidth = 1,
        #          padding = 10,
        #          foreground = colors[2],
        #          background = colors[1]
        #          ),
        #  widget.TextBox(
        #           font="FontAwesome",
        #          text="  ",
        #          foreground=colors[6],
        #          background=colors[1],
        #          padding = 0,
        #          fontsize=16
        #          ),
        #  widget.CPUGraph(
        #           border_color = colors[2],
        #           fill_color = colors[8],
        #           graph_color = colors[8],
        #           background=colors[1],
        #           border_width = 1,
        #           line_width = 1,
        #           core = "all",
        #           type = "box"
        #           ),
        widget.TextBox(text='',
                padding=8,
                fontsize=16,
                foreground=colors[9]),
        widget.CPU(format='{load_percent}%',
                foreground=colors[9]),
        widget.Sep(
                linewidth=0,
                padding=10,
                foreground=colors[2],
                background=colors[1]
                ),
        widget.TextBox(
                text="  ",
                foreground=colors[9],
                background=colors[1],
                padding=0,
                fontsize=16
                ),
        widget.Memory(
                font="FiraCode Nerd Font",
                format='{MemUsed: .0f}M /{MemTotal: .0f}M',
                update_interval=1,
                fontsize=12,
                foreground=colors[9],
                background=colors[1],
                ),
        widget.Sep(
                linewidth=0,
                padding=10,
                foreground=colors[2],
                background=colors[1]
                ),
        widget.TextBox(
                text="  ",
                foreground=colors[9],
                background=colors[1],
                padding=0,
                fontsize=16
                ),
        widget.Clock(
                foreground=colors[9],
                background=colors[1],
                fontsize=12,
                format="%a %d %b, %r"
                #format="%Y-%m-%d %H:%M"
                ),
        widget.Sep(
                linewidth=0,
                padding=10,
                foreground=colors[2],
                background=colors[1]
                ),
        widget.Systray(
                background=colors[1],
                icon_size=20,
                padding=4
                ),
    ]
    return widgets_list


widgets_list = init_widgets_list()


def init_widgets_screen1():
    widgets_screen1 = init_widgets_list()
    return widgets_screen1


def init_widgets_screen2():
    widgets_screen2 = init_widgets_list()
    del widgets_screen2[27::27]
    return widgets_screen2


widgets_screen1 = init_widgets_screen1()
widgets_screen2 = init_widgets_screen2()


def init_screens():
    return [Screen(top=bar.Bar(widgets=init_widgets_screen1(), size=26)),
            Screen(top=bar.Bar(widgets=init_widgets_screen2(), size=26))]


screens = init_screens()


# MOUSE CONFIGURATION
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size())
]

dgroups_key_binder = None
dgroups_app_rules = []

# ASSIGN APPLICATIONS TO A SPECIFIC GROUPNAME
# BEGIN

# @hook.subscribe.client_new
# def assign_app_group(client):
#     d = {}
#     #########################################################
#     ################ assgin apps to groups ##################
#     #########################################################
#     d["1"] = ["Navigator", "Firefox", "Vivaldi-stable", "Vivaldi-snapshot", "Chromium", "Google-chrome", "Brave", "Brave-browser",
#               "navigator", "firefox", "vivaldi-stable", "vivaldi-snapshot", "chromium", "google-chrome", "brave", "brave-browser", ]
#     d["2"] = [ "Atom", "Subl3", "Geany", "Brackets", "Code-oss", "Code", "TelegramDesktop", "Discord",
#                "atom", "subl3", "geany", "brackets", "code-oss", "code", "telegramDesktop", "discord", ]
#     d["3"] = ["Inkscape", "Nomacs", "Ristretto", "Nitrogen", "Feh",
#               "inkscape", "nomacs", "ristretto", "nitrogen", "feh", ]
#     d["4"] = ["Gimp", "gimp" ]
#     d["5"] = ["Meld", "meld", "org.gnome.meld" "org.gnome.Meld" ]
#     d["6"] = ["Vlc","vlc", "Mpv", "mpv" ]
#     d["7"] = ["VirtualBox Manager", "VirtualBox Machine", "Vmplayer",
#               "virtualbox manager", "virtualbox machine", "vmplayer", ]
#     d["8"] = ["Thunar", "Nemo", "Caja", "Nautilus", "org.gnome.Nautilus", "Pcmanfm", "Pcmanfm-qt",
#               "thunar", "nemo", "caja", "nautilus", "org.gnome.nautilus", "pcmanfm", "pcmanfm-qt", ]
#     d["9"] = ["Evolution", "Geary", "Mail", "Thunderbird",
#               "evolution", "geary", "mail", "thunderbird" ]
#     d["0"] = ["Spotify", "Pragha", "Clementine", "Deadbeef", "Audacious",
#               "spotify", "pragha", "clementine", "deadbeef", "audacious" ]
#     ##########################################################
#     wm_class = client.window.get_wm_class()[0]
#
#     for i in range(len(d)):
#         if wm_class in list(d.values())[i]:
#             group = list(d.keys())[i]
#             client.togroup(group)
#             client.group.cmd_toscreen()

# END
# ASSIGN APPLICATIONS TO A SPECIFIC GROUPNAME


main = None


@hook.subscribe.startup_once
def start_once():
    home = os.path.expanduser('~')
    subprocess.call([home + '/.config/qtile/scripts/autostart.sh'])


@hook.subscribe.startup
def start_always():
    # Set the cursor to something sane in X
    subprocess.Popen(['xsetroot', '-cursor_name', 'left_ptr'])


@hook.subscribe.client_new
def set_floating(window):
    if (window.window.get_wm_transient_for()
            or window.window.get_wm_type() in floating_types):
        window.floating = True


floating_types = ["notification", "toolbar", "splash", "dialog"]


follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(float_rules=[
    *layout.Floating.default_float_rules,
    Match(wm_class='Arcolinux-welcome-app.py'),
    Match(wm_class='Arcolinux-tweak-tool.py'),
    Match(wm_class='confirmreset'),
    Match(wm_class='makebranch'),
    Match(wm_class='maketag'),
    Match(wm_class='Arandr'),
    Match(wm_class='feh'),
    Match(wm_class='Galculator'),
    Match(wm_class='arcolinux-logout'),
    Match(wm_class='xfce4-terminal'),
    Match(title='branchdialog'),
    Match(title='Open File'),
    Match(title='pinentry'),
    Match(wm_class='ssh-askpass'),
    Match(wm_instance_class='Steam'),
    Match(wm_class='audacious'),
    Match(wm_class='protonvpn'),
    Match(wm_class='scrcpy'),
    Match(wm_class='steam'),
    Match(wm_class='xfce4-clipman-history'),
],  fullscreen_border_width=0, border_width=0)
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
respect_minimize_requests = True

focus_on_window_activation = "focus"  # or smart

wmname = "LG3D"
