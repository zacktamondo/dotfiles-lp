#!/bin/bash

function run {
	if ! pgrep $1; then
		$@ &
	fi
}

#Set your native resolution IF it does not exist in xrandr
#More info in the script
#run $HOME/.config/qtile/scripts/set-screen-resolution-in-virtualbox.sh

#Find out your monitor name with xrandr or arandr (save and you get this line)
#xrandr --output VGA-1 --primary --mode 1360x768 --pos 0x0 --rotate normal
#xrandr --output eDP-1 --primary --mode 1920x1080 --rate 60.0&
#xrandr --output LVDS1 --mode 1366x768 --output DP3 --mode 1920x1080 --right-of LVDS1
#xrandr --output HDMI2 --mode 1920x1080 --pos 1920x0 --rotate normal --output HDMI1 --primary --mode 1920x1080 --pos 0x0 --rotate normal --output VIRTUAL1 --off

#change your keyboard if you need it
#setxkbmap -layout be

#autostart ArcoLinux Welcome App
#run dex $HOME/.config/autostart/arcolinux-welcome-app.desktop &

#Some ways to set your wallpaper besides variety or nitrogen
# feh --no-fehbg --bg-fill ~/Documents/Wallpaper/Ehsan/laptop.png &
# feh --no-fehbg --bg-fill ~/Pictures/mgs.jpg &
#start the conky to learn the shortcuts
#(conky -c $HOME/.config/conky/breaking-bad/rings-v1.2.1_breaking_bad.lua) &

#IN BETA PHASE
#start sxhkd to replace Qtile native key-bindings
#run sxhkd -c ~/.config/qtile/sxhkd/sxhkdrc &

picom --daemon &
# Set Display settings
$HOME/.screenlayout/internal_L_extrnal_R.sh &
killall picom &
sleep 1
picom --daemon &

#starting utility applications at boot time
run variety &
run nm-applet &
run xfce4-power-manager &
run xfce4-screensaver & # to enable xflock4
numlockx on &
blueberry-tray &
/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &
/usr/lib/xfce4/notifyd/xfce4-notifyd &
setxkbmap -option grp:switch,grp:alt_shift_toggle us,ara &
xfce4-clipman &
xinput set-prop "CUST0001:00 04F3:30AA Touchpad" "libinput Tapping Enabled" 1 &
run xclip &
run volumeicon &
#run mechvibes &
run syncthing -no-browser &
/usr/bin/emacs --daemon &
/usr/lib/pam_kwallet_init &
